# SS-LOEC Fseq4py

A Python implementation of F-sequences.


## Contributions

At the moment, we do not have a way to receive contributions to the project.

As soon as possible, we will open some means of communication to receive contributions and thank you for them.


## Authors

- [@IsraelBuitronD](https://www.gitlab.com/IsraelBuitronD) Israel Buitron-Damaso
- [@quiqev42](https://www.gitlab.com/quiqev42) Luis Enrique Ortiz-Camacho


## License

No license for use by third parties.
All rights reserved.
