# SS-LOEC Fseq4py

Una implementación en Python de las sucesiones-F.


## Contribuciones

Por el momento, no tenemos un medio para recibir contribuciones al proyecto.

A la brevedad posible, abriremos algún medio de comunicación para recibir contribuciones y agradecérlas.


## Autores

- [@IsraelBuitronD](https://www.gitlab.com/IsraelBuitronD) Israel Buitrón Dámaso
- [@quiqev42](https://www.gitlab.com/quiqev42) Luis Enrique Ortiz Camacho


## Licencia

Sin licencia de uso alguno para terceros.
Todos los derechos están reservados.
